
import "./App.css"

import img from "./assets/images/48.jpg"
function App() {
  return (
    <div className="dev-container">
      <div>
        <img src={img} alt="ảnh" className="dev-avt"></img>
      </div>
      <div className="dev-word">
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className="dev-user">
      <b>Tammy Stevens</b> * Front End developer 
      </div>
    </div>
  );
}

export default App;